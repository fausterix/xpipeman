Please read README.ubuntu for instructions to compile

------

Xpipeman is a game of skill requiring the user to connect together
pieces of a pipe to allow a liquid to flow through without
leaking out. The aim of the game is to connect as many pieces of pipe 
to the start block as possible before the liquid flows out of the
end on the pipe (see manual page for further details).

------

  Copyright 1991 Nigel Paver
  
  Permission to use, copy, modify, distribute, and sell this software and its
  documentation for any purpose is hereby granted without fee, provided that
  the above copyright notice appear in all copies and that both that
  copyright notice and this permission notice appear in supporting
  documentation, and that the author's name not be used in advertising or
  publicity pertaining to distribution of the software without specific,
  written prior permission.  The author makes no representations about the
  suitability of this software for any purpose.  It is provided "as is"
  without express or implied warranty.
  
  THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING 
  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE 
  AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY 
  DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN 
  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF 
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
